package laurier.cvm.lifedata;


import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.concurrent.TimeUnit;

import laurier.cvm.lifedata.demo.DataSpoofingActivity;
import laurier.cvm.lifedata.demo.SpoofingModel;

public class MainActivity extends AppCompatActivity {
    private static boolean authorized = false;
    private TextView textViewLogoApplication;
    private TextView textViewEnterPhoneNumber;
    private Button btnSumbitPhoneNumber;
    private EditText editTextPhoneNumber;
    private DatabaseReference clientsDatabase;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public static void setAuthorized(boolean value){
        authorized = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_life_data);
        //setup database
        clientsDatabase = FirebaseDatabase.getInstance().getReference("clients");

        sharedPreferences = getApplicationContext().getSharedPreferences("LifeDataPrefs", 0); // 0 - for private mode
        editor = sharedPreferences.edit();
        if(sharedPreferences.getString("telephone", "") != ""){
            authorized = true;
        }
        if(authorized){
            startActivity(new Intent(MainActivity.this, DataSpoofingActivity.class));
            overridePendingTransition(R.transition.enter_from_right, R.transition.exit_out_left);

            //Intent intent = new Intent(MainActivity.this, DataSpoofingActivity.class);
            //startActivity(intent);
        }

        //get View objects
        textViewEnterPhoneNumber = findViewById(R.id.textView_enter_firstname);
        textViewLogoApplication = findViewById(R.id.textView_logo);
        btnSumbitPhoneNumber = findViewById(R.id.btn_submit_phone);
        editTextPhoneNumber = findViewById(R.id.editText_firstname);

        //set Typeface on UI Elements
        Typeface face;
        face = Typeface.createFromAsset(getAssets(), "fonts/raleway_regular.ttf");
        textViewLogoApplication.setTypeface(face);
        textViewEnterPhoneNumber.setTypeface(face);
        btnSumbitPhoneNumber.setTypeface(face);


        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks(){

            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                String telephoneNum = editTextPhoneNumber.getText().toString();
                editor.putString("telephone",telephoneNum ); // Storing string
                editor.commit(); // commit changes
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference("alerts");
                String phone_number = getApplicationContext().getSharedPreferences("LifeDataPrefs", 0).getString("telephone", "");
                try {
                    ref.child(phone_number).child("instanceId").setValue(FirebaseInstanceId.getInstance().getToken());
                }catch(Exception e){
                    Toast t = Toast.makeText(getApplicationContext(), "error updating instance ID in server", Toast.LENGTH_SHORT);
                }
                Intent i = new Intent(MainActivity.this, PersonalInformationActivity.class);
                startActivity(i);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Toast t = Toast.makeText(MainActivity.this, "verification failed", Toast.LENGTH_SHORT);
                t.show();
            }
        };

        btnSumbitPhoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(authorized){
                    Intent i = new Intent(MainActivity.this, DataSpoofingActivity.class);
                    startActivity(i);
                }
                else if(editTextPhoneNumber.getText().toString().matches("([0-9]{10})|(([0-9]{3}-){2}[0-9]{4})")){
                    Toast t = Toast.makeText(getApplicationContext(), "sending verification code...", Toast.LENGTH_SHORT);
                    t.show();
                    PhoneAuthProvider.getInstance().verifyPhoneNumber(
                            editTextPhoneNumber.getText().toString(),        // Phone number to verify
                            30,                 // Timeout duration
                            TimeUnit.SECONDS,   // Unit of timeout
                            MainActivity.this,               // Activity (for callback binding)
                            mCallbacks);        // OnVerificationStateChangedCallbacks
                }
            }
        });

        getSupportActionBar().hide();
    }
}



