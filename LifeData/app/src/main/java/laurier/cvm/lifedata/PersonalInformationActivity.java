package laurier.cvm.lifedata;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PersonalInformationActivity extends AppCompatActivity {
    private Button btnSumbitNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_information);

        //set Typeface on UI Elements
        Typeface face;
        face = Typeface.createFromAsset(getAssets(), "fonts/raleway_regular.ttf");
        btnSumbitNext = findViewById(R.id.btn_submit_spoof);
        btnSumbitNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MainActivity.setAuthorized(true);
                startActivity(new Intent(PersonalInformationActivity.this, EmergencyActivity.class));
                overridePendingTransition(R.transition.enter_from_right, R.transition.exit_out_left);
            }
        });
        getSupportActionBar().hide();
    }
}
