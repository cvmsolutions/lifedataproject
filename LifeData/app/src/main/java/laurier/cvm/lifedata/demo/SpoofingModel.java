package laurier.cvm.lifedata.demo;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by Laurier on 2018-02-04.
 */

public class SpoofingModel {
    private static SpoofingModel instance;
    public static SpoofingModel getInstance(){
        if(instance == null){
            instance = new SpoofingModel();
        }
        return instance;
    }

    private ArrayList<SpoofedEntry> spoofedEntries;
    private ArrayList<String> spoofingIds;


    private SpoofingModel(){
        spoofedEntries = new ArrayList<SpoofedEntry>();
        spoofingIds = new ArrayList<String>();
    }
    public List<String> getSpoofingIds(){
        return spoofingIds;
    }
    public void addSpoofedEntry(SpoofedEntry newEntry){
        spoofedEntries.add(newEntry);
        spoofingIds.add(newEntry.getEntryId());
    }
    public void spoof(){
        for (SpoofedEntry s :
                spoofedEntries) {
            s.spoof();
        }
    }
    public void quitSpoofing(){
        for (SpoofedEntry s :
                spoofedEntries) {
            s.quitSpoofing();
        }
        spoofedEntries = new ArrayList<SpoofedEntry>();
    }
}
