package laurier.cvm.lifedata.demo;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.concurrent.ThreadLocalRandom;

import laurier.cvm.lifedata.MainActivity;

/**
 * Created by Laurier on 2018-02-04.
 */

public class SpoofedEntry {

    private String entryId;
    private DatabaseReference reference;

    private double lat;
    private double lng;
    private int hr;

    public SpoofedEntry(){
        do{
            String newId = "";
            for(int i = 0; i < 10; i++){
                int randomNum = (int)(Math.random()*10)%10;
                newId+=String.valueOf(randomNum);
            }
            entryId = newId;
            System.out.println(getEntryId());
        }while(SpoofingModel.getInstance().getSpoofingIds().contains(entryId));

        reference = FirebaseDatabase.getInstance().getReference("alerts").child(entryId);
        updateDatabaseValue();
    }
    private int randomizeHeartrate(){
        return (int)(Math.random()*20)+ 60;
    }
    private double randomizeLat(){
        return lat = Math.random()*50;
    }
    private double randomizeLng(){
        return lat = Math.random()*20;
    }
    private void updateDatabaseValue(){
        lat = randomizeLat();
        lng = randomizeLng();
        hr = randomizeHeartrate();

        reference.child("latitude").setValue(lat);
        reference.child("longitude").setValue(lng);
        reference.child("heartrate").setValue(hr);
    }
    public String getEntryId() {
        return entryId;
    }
    public void spoof(){
        //something
        updateDatabaseValue();
    }
    public void quitSpoofing(){
        DatabaseReference alertsRoot = FirebaseDatabase.getInstance().getReference("alerts");
        alertsRoot.child(entryId).removeValue();
    }
}
