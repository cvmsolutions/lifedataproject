package laurier.cvm.lifedata.demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import laurier.cvm.lifedata.R;

public class DataSpoofingActivity extends AppCompatActivity {

    private Button buttonAddSpoofing;
    private Button buttonQuitSpoofing;
    private Button buttonSpoof;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //generate 5 random spoofing entries
        /*
        for(int i = 0; i < 5; i++){
            SpoofingModel.getInstance().addSpoofedEntry(new SpoofedEntry());
        }*/
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_spoofing);

        buttonQuitSpoofing = findViewById(R.id.btn_quit_spoofing);
        buttonSpoof = findViewById(R.id.btn_spoof);
        buttonAddSpoofing = findViewById(R.id.btn_add_spoofing_data);



        //button configured to add data spoofing entries
        buttonAddSpoofing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SpoofingModel.getInstance().addSpoofedEntry(new SpoofedEntry());
            }
        });
        buttonSpoof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SpoofingModel.getInstance().spoof();
            }
        });
        //button configured to qui spoofing
        buttonQuitSpoofing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SpoofingModel.getInstance().quitSpoofing();
            }
        });
    }
}
