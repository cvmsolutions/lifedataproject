package laurier.cvm.lifedata;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import laurier.cvm.lifedata.R;
import laurier.cvm.lifedata.demo.DataSpoofingActivity;

public class EmergencyActivity extends AppCompatActivity {

    private Button emergency, spoof;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.emergency_acitivity);
        emergency = (Button) findViewById(R.id.emergency);
        spoof = (Button) findViewById(R.id.btn_submit_spoof);
        //set Typeface on UI Elements
        Typeface face;
        face = Typeface.createFromAsset(getAssets(), "fonts/raleway_regular.ttf");
        emergency.setTypeface(face);

        emergency.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

            }
        });

        spoof.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                startActivity(new Intent(EmergencyActivity.this, DataSpoofingActivity.class));
                overridePendingTransition(R.transition.enter_from_right, R.transition.exit_out_left);
            }
        });
        getSupportActionBar().hide();
    }
}
