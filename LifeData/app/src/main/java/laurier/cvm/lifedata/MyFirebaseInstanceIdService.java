package laurier.cvm.lifedata;

import android.content.SharedPreferences;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Laurier on 2018-02-04.
 */

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        updateRegistrationInServer(refreshedToken);
    }

    private void updateRegistrationInServer(String refreshedToken) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("alerts");
        String phone_number = getApplicationContext().getSharedPreferences("LifeDataPrefs", 0).getString("telephone", "");
        try {
            ref.child(phone_number).child("instanceId").setValue(FirebaseInstanceId.getInstance().getToken());
        }catch(Exception e){
            Toast t = Toast.makeText(getApplicationContext(), "error updating instance ID in server", Toast.LENGTH_SHORT);
        }
    }
}
